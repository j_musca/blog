require 'test_helper'

class PostTest < ActiveSupport::TestCase

  test "should not save post without title" do
    post = Post.new
    post.body = "Very nice body"
    assert_not post.save
  end

  test "should not save post without body" do
    post = Post.new
    post.title = "Very nice title"
    assert_not post.save
  end

  test "should save post with body and title" do
    post = Post.new
    post.title = "Very nice title"
    post.body = "Very nice body"
    assert post.save
  end

end
